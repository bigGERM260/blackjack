﻿using Blackjack.Model;
using Blackjack.View_Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
    
namespace Blackjack.View
{
    /// <summary>
    /// Interaction logic for Main.xaml
    /// </summary>
    public partial class Main : Window
    {
        // Properties
        private static Mutex mutex;
        MainViewModel mainModel;

        //List<Card> deck = new List<Card>();


        public Main()
        {
            try
            {
                bool created;
                mutex = new Mutex(false, "Blackjack", out created);

                if (!created)
                { // prevent user from opening multiple instances of the application
                    MessageBox.Show("Blackjack is already running.  Only one instance of this application is allowed at once.",
                        "Application Already Running!", MessageBoxButton.OK, MessageBoxImage.Information);
                    Environment.Exit(0);
                    return;
                }

                // initialize
                InitializeComponent();
                mainModel = new MainViewModel(this);
                DataContext = mainModel;
            }
            catch (Exception ex)
            {
                MessageBox.Show(String.Format("Blackjack encountered an error!  Application terminated!\n\nEx: {0}", ex.Message), "Blackjack Error!",
                    MessageBoxButton.OK, MessageBoxImage.Error);
            }
            
        }
    }
}
