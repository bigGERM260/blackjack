﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Blackjack.View
{
    /// <summary>
    /// Interaction logic for PieChart.xaml
    /// </summary>
    public partial class PieChart : UserControl  
    {
        public PieChart(int currentRate, int historicRate)
        {
            InitializeComponent();
            // curent data
            this.pieChartCurrent.Value = (currentRate < 0) ? 0 : currentRate;
            this.pieChartCurrent.StrokeThickness = 3;


            // record book data
            this.pieChartRecord.Value = (historicRate < 0) ? 0 : historicRate;
            this.pieChartRecord.StrokeThickness = 3;
        }
    }
}
