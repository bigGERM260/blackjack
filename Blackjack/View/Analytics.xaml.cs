﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Blackjack.Model;
using Blackjack.ViewModel;

namespace Blackjack.View
{
    /// <summary>
    /// Interaction logic for Analytics.xaml
    /// </summary>
    public partial class Analytics : Window
    {
        AnalyticsViewModel analyticsViewModel;

        public Analytics(Player User, Player Dealer, String path)
        {
            try
            {
                // initialize
                InitializeComponent();
                analyticsViewModel = new AnalyticsViewModel(this, User, Dealer, path);
                DataContext = analyticsViewModel;
            }
            catch (Exception ex)
            {
                MessageBox.Show(String.Format("Blackjack Analytics encountered an error!  The window failed to open!\n\nEx: {0}", ex.Message), "Blackjack Analytics Error!",
                    MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }
    }
}
