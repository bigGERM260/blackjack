﻿using Blackjack.View_Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace Blackjack.ViewModel.Commands
{
    class HitMeCommand : ICommand
    {
        MainViewModel parent;
        public HitMeCommand(MainViewModel parent)
        {
            this.parent = parent;
            parent.PropertyChanged += delegate { CanExecuteChanged?.Invoke(this, EventArgs.Empty); };
        }

        public event EventHandler CanExecuteChanged;
        public bool CanExecute(object parameter)
        {
            if (parent.User.MY_TURN && !parent.Dealer.BUST && !parent.User.BUST)
                return true;
            else
                return false;
        }

        /// <summary>
        /// This gives the user another card from the deck
        /// </summary>
        /// <param name="parameter"></param>
        public void Execute(object parameter)
        {
            parent.HitUser();
        }

    }
}
