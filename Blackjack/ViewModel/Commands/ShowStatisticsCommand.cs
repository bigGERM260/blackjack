﻿using Blackjack.View_Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace Blackjack.ViewModel.Commands
{
    class ShowStatisticsCommand : ICommand
    {
        MainViewModel parent;
        public ShowStatisticsCommand(MainViewModel parent)
        {
            this.parent = parent;
            parent.PropertyChanged += delegate { CanExecuteChanged?.Invoke(this, EventArgs.Empty); };
        }

        public event EventHandler CanExecuteChanged;
        public bool CanExecute(object parameter)
        {
            return true;
        }

        /// <summary>
        /// This gives the user another card from the deck
        /// </summary>
        /// <param name="parameter"></param>
        public void Execute(object parameter)
        {
            parent.ShowHideStatisticalBreakdown();
        }

    }
}
