﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;
using Blackjack.Model;
using Blackjack.View;
using Json.Net;
using Jsonn = System.Text.Json;
using System.Text.Json.Serialization;
using LiveCharts.Wpf;
using Newtonsoft.Json;

/// <summary>
/// This is needed for MVVM features to work on .NET Framework 4.0
/// </summary>
//namespace System.Runtime.CompilerServices
//{
//    sealed class CallerMemberNameAttribute : Attribute { }
//}


namespace Blackjack.ViewModel
{
    class AnalyticsViewModel : INotifyPropertyChanged
    {
        // ----------------------------------------------------------------------------------------
        // -------------------------------------- Properties --------------------------------------

        public Analytics analyticsWindow;

        private Player _dealer;
        public Player Dealer
        {
            get { return _dealer; }
            set
            {
                if (value == _dealer)
                    return;
                _dealer = value;
                OnPropertyChanged("Dealer");
            }
        }

        private Player _user;
        public Player User
        {
            get { return _user; }
            set
            {
                if (value == _user)
                    return;
                _user = value;
                OnPropertyChanged("User");
            }
        }

        private View.PieChart _graphContent = new View.PieChart(0, 0);
        public View.PieChart graphContent
        {
            get { return _graphContent; }
            set
            {
                _graphContent = value;
                this.OnPropertyChanged("graphContent");
            }
        }


        // file variables
        private string path;
        private const string fileName = "Blackjack_RecordBook.json";
        

 

        // -------------------------------------- Properties --------------------------------------
        // ----------------------------------------------------------------------------------------
        // ------------------------------------ Initialization ------------------------------------

        /// <summary>
        /// This is the constructor
        /// </summary>
        /// <param name="Analytics"></param>
        public AnalyticsViewModel(Analytics analyticsWindow, Player User, Player Dealer, String path)
        {
            InitializeCommands();

            // initialization
            this.analyticsWindow = analyticsWindow;
            this.User = User;
            this.Dealer = Dealer;
            this.path = path;

            GatherChartData();
        }


        /// <summary>
        /// This initializes the commands
        /// </summary>
        public void InitializeCommands()
        {
            CloseWindowCommand = new ViewModel.Commands.CloseWindowCommand(this);
        }


        // ------------------------------------ Initialization ------------------------------------
        // ----------------------------------------------------------------------------------------
        // --------------------------------------- Functions --------------------------------------


        /// <summary>
        /// This gathers the data needed to generate the graph of W/L history
        /// </summary>
        public void GatherChartData()
        {
            // object state
            Player dealer_json = new Player("Dealer");
            Player user_json = new Player("User");

            // read statistics from record books
            String jsonRaw;
            using (StreamReader stream = new StreamReader(path))
            {
                jsonRaw = stream.ReadToEnd();

                if (jsonRaw.Length > 0)
                {
                    using (var ms = new MemoryStream(Encoding.Unicode.GetBytes(jsonRaw)))
                    {
                        List<Player> players_json = JsonConvert.DeserializeObject<List<Player>>(jsonRaw);
                        dealer_json = players_json.First(x => x.NAME == "Dealer");
                        user_json = players_json.First(x => x.NAME == "User");
                    }
                }
            } 

            double currentRate = User.WINS / (double)(User.WINS + User.LOSES) * 100;
            double historicRate = user_json.WINS / (double)(user_json.WINS + user_json.LOSES) * 100;
            var graph = new View.PieChart((int)currentRate, (int)historicRate);


            // displaly the graph
            this.graphContent = graph;
        }

        
        // -------------------------------------- Functions ---------------------------------------
        // ----------------------------------------------------------------------------------------
        // ---------------------------------------- Events ----------------------------------------

        public void CloseWindow()
        {
            this.analyticsWindow.Close();
        }


        // ---------------------------------------- Events ----------------------------------------
        // ----------------------------------------------------------------------------------------
        // -------------------------------------- MVC/MVVM ----------------------------------------

        #region INotify
        public event PropertyChangedEventHandler PropertyChanged;

        void OnPropertyChanged([CallerMemberName] String propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }

        #endregion

        #region ICommandEvents
        public ICommand CloseWindowCommand { get; private set; }

        #endregion

        // -------------------------------------- MVC/MVVM ----------------------------------------
        // ----------------------------------------------------------------------------------------
    }
}
