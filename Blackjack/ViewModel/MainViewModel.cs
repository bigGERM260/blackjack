﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;
using Blackjack.Model;
using Blackjack.View;
using Json.Net;
using Jsonn = System.Text.Json;
using System.Text.Json.Serialization;
using LiveCharts.Wpf;
using Newtonsoft.Json;
using System.Windows.Controls;
using System.Windows.Media.Imaging;
using System.Threading;
using System.Windows.Media;
using System.Windows.Data;
using System.Windows.Threading;
using System.Reflection;

/// <summary>
/// This is needed for MVVM features to work on .NET Framework 4.0
/// </summary>
//namespace System.Runtime.CompilerServices
//{
//    sealed class CallerMemberNameAttribute : Attribute { }
//}


namespace Blackjack.View_Model
{
    class MainViewModel : INotifyPropertyChanged
    {
        // ----------------------------------------------------------------------------------------
        // -------------------------------------- Properties --------------------------------------

        public Main mainWindow;

        private Player _dealer;
        public Player Dealer
        {
            get { return _dealer; }
            set
            {
                if (value == _dealer)
                    return;
                _dealer = value;
                OnPropertyChanged("Dealer");
            }
        }

        private Player _user;
        public Player User
        {
            get { return _user; }
            set
            {
                if (value == _user)
                    return;
                _user = value;
                OnPropertyChanged("User");
            }
        }

        private Visibility _showStatistics;
        public Visibility ShowStatistics
        {
            get { return _showStatistics; }
            set
            {
                if (value == _showStatistics)
                    return;
                _showStatistics = value;
                OnPropertyChanged("ShowStatistics"); 
            }
        }

        private View.PieChart _graphContent = new View.PieChart(0, 0);
        public View.PieChart graphContent
        {
            get { return _graphContent; }
            set
            {
                _graphContent = value;
                this.OnPropertyChanged("graphContent");
            }
        }

        public DeckOfCards Deck;
        public AI Smarty;
        public int DeckIndex;

        // file variables
        private String directory;
        private string path;
        private const string fileName = "Blackjack_RecordBook.json";
        

        // constants
        const int INITIAL_CARDS = 2;

        // -------------------------------------- Properties --------------------------------------
        // ----------------------------------------------------------------------------------------
        // ------------------------------------ Initialization ------------------------------------

        /// <summary>
        /// This is the constructor
        /// </summary>
        /// <param name="mainWindow"></param>
        public MainViewModel(Main mainWindow)
        {
            InitializeCommands();
            this.mainWindow = mainWindow;

            // set working directory
            string currentDirectory = Environment.CurrentDirectory;
            this.directory = Directory.GetParent(currentDirectory).ToString();
            this.path = Path.Combine(directory, fileName);

            //initialization
            NewMatch();
        }

        /// <summary>
        /// This initializes various components of a the game
        /// </summary>
        public void InitializeGameComponents()
        {
            // game initialization
            Dealer.NewGame();
            User.NewGame();
            User.ToggleTurn();
            Deck = new DeckOfCards();
            Smarty = new AI(Dealer, User);
            DeckIndex = 0;
            ShowStatistics = Visibility.Hidden;

            // deal initial cards
            for (int count = 0; count < INITIAL_CARDS; count++)
            {
                HitUser();
                HitDealer();
                
            }
        }


        /// <summary>
        /// This reinitializes the commands
        /// </summary>
        public void InitializeCommands()
        {
            HitMeCommand = new ViewModel.Commands.HitMeCommand(this);
            NewGameCommand = new ViewModel.Commands.NewGameCommand(this);
            StayCommand = new ViewModel.Commands.StayCommand(this);
            SaveCommand = new ViewModel.Commands.SaveCommand(this);
            ShowStatisticsCommand = new ViewModel.Commands.ShowStatisticsCommand(this);
        }


        // ------------------------------------ Initialization ------------------------------------
        // ----------------------------------------------------------------------------------------
        // --------------------------------------- Functions --------------------------------------

        /// <summary>
        /// This starts a new game between the user and the dealer
        /// </summary>
        public void NewGame()
        {
            UpdateWins();
            InitializeGameComponents();
            // clear GUI cards
            ClearUICards();
        }

        /// <summary>
        /// This resets the entire match
        /// </summary>
        public void NewMatch()
        {
            //initialization
            Dealer = new Player("Dealer");
            User = new Player("User")
            {
                MY_TURN = true
            };

            InitializeGameComponents();
            InitializeCommands();
            // clear GUI cards
            ClearUICards();
        }

        /// <summary>
        /// This updates the json record keeping book
        /// </summary>
        public void Save()
        {
            // object state
            Player dealer_json = new Player("Dealer");  
            Player user_json = new Player("User");

            // create the file, if necessary
            if (!File.Exists(path))
            {
                File.Create(path).Close();
                dealer_json = new Player("Dealer");
                user_json = new Player("User");
            }
            else
            {
                String jsonRaw;
                using (StreamReader stream = new StreamReader(path))
                {
                    jsonRaw = stream.ReadToEnd();
                    
                    if (jsonRaw.Length > 0)
                    {
                        List<Player> players_json = JsonConvert.DeserializeObject<List<Player>>(jsonRaw);
                        dealer_json = players_json.First(x => x.NAME == "Dealer");
                        user_json = players_json.First(x => x.NAME == "User");
                    }
                }

                //if file is empty, delete and recreate
                if (jsonRaw.Length < 1)
                {
                    File.Delete(path);
                    Save();
                    return;
                }

            }

            // combine scores
            user_json.WINS = user_json.WINS + this.User.WINS;
            user_json.LOSES = user_json.LOSES + this.User.LOSES;

            dealer_json.WINS = dealer_json.WINS + this.Dealer.WINS;
            dealer_json.LOSES = dealer_json.LOSES + this.Dealer.LOSES;

            List<Player> players = new List<Player>();
            players.Add(dealer_json);
            players.Add(user_json);


            // write 
            using (StreamWriter writer = new StreamWriter(path, false))
            {
                String jsonData = Jsonn.JsonSerializer.Serialize(players);
                writer.Write(jsonData);
            }

            // reset the game after a save
            NewMatch();
        }


        /// <summary>
        /// This gathers the data needed to generate the graph of W/L history
        /// </summary>
        public void ShowHideStatisticalBreakdown()
        {
            Analytics analytics = new Analytics(User, Dealer, path);
            //mainWindow.Hide();
            analytics.ShowDialog();
            //mainWindow.Show();
            return;
        }


        public void HitDealer_Async()
        {
            HitDealer();
        }

        /// <summary>
        /// This deals a card to the dealer
        /// </summary>
        public void HitDealer()
        {
            // add to players hand
            if (Dealer.MY_TURN)
            {
                if (User.BUST)
                    return;

                if (Dealer.HAND.Count >= 2)
                    System.Threading.Thread.Sleep(1500);

                if (Smarty.HitOrNah())
                {
                    AceHighOrLow();
                    Dealer.AddCardToHand(Deck.Deck[DeckIndex]);
                    AddCardToGUI_Dealer(Deck.Deck[DeckIndex]);
                    DeckIndex++;
                }
            }

            ChangeTurn();
        }


        /// <summary>
        /// This deals a card to the user and calls the AI to 
        /// decide to hit or stay for the dealer
        /// </summary>
        public async Task HitUser()
        { 
            if (User.MY_TURN)
            {
                AceHighOrLow();
                User.AddCardToHand(Deck.Deck[DeckIndex]);
                AddCardToGUI_User(Deck.Deck[DeckIndex]);

                DeckIndex++;
                ChangeTurn();

                if (Dealer.HAND.Count >= 2)
                {
                    await Task.Run(() => HitDealer_Async());
                }
            }
        }

        /// <summary>
        /// This passes the users turn to the dealer
        /// </summary>
        public async void UserStay()
        {
            if (User.MY_TURN)
            {
                ChangeTurn();
                await Task.Run(() => HitDealer_Async());
            }
        }

        /// <summary>
        /// This determines whos turn it is to be dealt a card
        /// </summary>
        public void ChangeTurn()
        {
            Dealer.ToggleTurn();
            User.ToggleTurn();
        }

        /// <summary>
        /// This awards the latest victory to the appropriate player
        /// </summary>
        public void UpdateWins()
        {
            // if tie game OR hand not fully played
            if (User.SCORE == Dealer.SCORE || (!User.BUST && !Dealer.BUST))
            {
                // do nothing
                return;
            }
            // if user wins
            if (Dealer.BUST ||  (!User.BUST &&User.SCORE > Dealer.SCORE))
            {
                User.WINS++;
                Dealer.LOSES++;
            }// if dealer wins
            else if  (User.BUST || (!Dealer.BUST && User.SCORE < Dealer.SCORE))
            {
                User.LOSES++;
                Dealer.WINS++;
            }
        }

        /// <summary>
        /// This adds the newly dealt card to the players hand (graphically)
        /// </summary>
        public void AddCardToGUI_Dealer(Card newCard)
        {
            //Application.Current.Dispatcher.
            Application.Current.Dispatcher.BeginInvoke(new Action(() =>
            {
                int verticleOffset = 20;
                int horizontalOffset = 40;

                double offsetFactor = (Dealer.HAND.Count / 2.0) - .5;
                int index = 0;

                // update position of all dealer cards
                ClearUIHand_Dealer();
                foreach (Card c in Dealer.HAND)
                {
                    var imgCard = MakeCard(c, index);
                    // position card
                    Canvas.SetLeft(imgCard, this.mainWindow.canDealer.Width / 2 - (imgCard.Width / 2) - (horizontalOffset * offsetFactor));
                    Canvas.SetTop(imgCard, ((this.mainWindow.canDealer.Height / 2) - 40) - (imgCard.Width / 2) - (verticleOffset * offsetFactor));
                    this.mainWindow.canDealer.Children.Add(imgCard);
                    // increment
                    offsetFactor--;
                    index++;
                }
            }), DispatcherPriority.Normal);
        }

        /// <summary>
        /// This adds the newly dealt card to the players hand (graphically)
        /// </summary>
        public void AddCardToGUI_User(Card newCard)
        {
            //Application.Current.Dispatcher.
            Application.Current.Dispatcher.BeginInvoke(new Action(() =>
            {
                int verticleOffset = 20;
                int horizontalOffset = 40;

                double offsetFactor = (User.HAND.Count / 2.0) - .5;
                int index = 0;

                // update position of all dealer cards
                ClearUIHand_User();
                foreach (Card c in User.HAND)
                {
                    var imgCard = MakeCard(c, index);
                    // position card
                    Canvas.SetLeft(imgCard, this.mainWindow.canUser.Width / 2 - (imgCard.Width / 2) - (horizontalOffset * offsetFactor));
                    Canvas.SetTop(imgCard, ((this.mainWindow.canUser.Height / 2) - 40) - (imgCard.Width / 2) - (verticleOffset * offsetFactor));
                    this.mainWindow.canUser.Children.Add(imgCard);
                    // increment
                    offsetFactor--;
                    index++;
                }
            }), DispatcherPriority.Normal);
        }

        /// <summary>
        /// 
        /// </summary>
        public void AceHighOrLow()
        {
            if (User.MY_TURN && User.HAND.Count >= 2 &&
                (Deck.Deck[DeckIndex].Rank == Card.RankEnum.AceHigh
                || Deck.Deck[DeckIndex].Rank == Card.RankEnum.AceLow))
            {
                if (MessageBox.Show("Ace can be played as either 1 or 11.  Would you like to play your Ace as 11? \n\n(Yes = 11, No = 1)",
                    "Ace High or Ace Low?",
                    MessageBoxButton.YesNo,
                    MessageBoxImage.Question) == MessageBoxResult.Yes)
                {
                    Deck.Deck[DeckIndex].SetAceHigh();
                }
                else
                    Deck.Deck[DeckIndex].SetAceLow();
            }
            else // Dealers turn
            {
                if (Dealer.SCORE + 11 <= 21)
                    Deck.Deck[DeckIndex].SetAceHigh();
                else
                    Deck.Deck[DeckIndex].SetAceLow();
            }
        }

        // -------------------------------------- Functions ---------------------------------------
        // ----------------------------------------------------------------------------------------
        // ---------------------------------------- Helper ----------------------------------------

        /// <summary>
        /// Remove the GUI cards for both the user and dealer
        /// </summary>
        public void ClearUICards()
        {

            var userChildren = "";
            var dealerChildren = "";

            foreach (var child in this.mainWindow.canUser.Children)
            {
                userChildren += child.ToString() + ", ";
            }

            foreach (var child in this.mainWindow.canDealer.Children)
            {
                dealerChildren += child.ToString() + ", ";
            }

            ClearUIHand_Dealer();
            ClearUIHand_User();
        }

        /// <summary>
        /// This removes all of the dealers cards from the UI
        /// </summary>
        public void ClearUIHand_Dealer()
        {
            // clear canvas
            this.mainWindow.canDealer.Children.Clear();
            var lblBusted = CreateBustedLabel();
            lblBusted.SetBinding(Label.VisibilityProperty, new Binding("Dealer.BUST_VISIBILITY"));
            // add label to canvas
            this.mainWindow.canDealer.Children.Add(lblBusted);
        }

        /// <summary>
        /// This removes all of the users cards from the UI
        /// </summary>
        public void ClearUIHand_User()
        {
            // clear canvas
            this.mainWindow.canUser.Children.Clear();
            var lblBusted = CreateBustedLabel();
            lblBusted.SetBinding(Label.VisibilityProperty, new Binding("User.BUST_VISIBILITY"));
            // add label to canvas
            this.mainWindow.canUser.Children.Add(lblBusted);
        }

        /// <summary>
        /// This makes the "BUSTED!" label for the canvas.
        /// Note: Clearing specific children/indexes of canvas is unpredictable, so deleting the
        /// images and trying to preserve the label doesn't work as desired.
        /// </summary>
        /// <returns></returns>
        public Label CreateBustedLabel()
        {
            Label lblBusted = new Label()
            {
                Content = "BUSTED!",
                Foreground = new SolidColorBrush(Colors.Red),
                FontSize = 70,
                Width = 400,
                FontWeight = FontWeights.ExtraBold,
                HorizontalContentAlignment = HorizontalAlignment.Center
            };
            Canvas.SetTop(lblBusted, 100);
            Canvas.SetZIndex(lblBusted, 1);

            return lblBusted;
        }

        /// <summary>
        /// This makes the necessary card image to add to the user/dealers hand
        /// </summary>
        /// <param name="index"></param>
        /// <returns></returns>
        public Image MakeCard(Card newCard, int index)
        {
            var imgCard = new Image();
            BitmapImage bmp = new BitmapImage();
            bmp.BeginInit();
            bmp.UriSource = new Uri(GetCardImage(newCard), UriKind.Relative); 
            bmp.EndInit();
            imgCard.Name = "imgDealer_" + index;
            imgCard.Source = bmp;
            imgCard.Stretch = System.Windows.Media.Stretch.Fill;
            imgCard.HorizontalAlignment = HorizontalAlignment.Left;
            imgCard.Height = 150;
            imgCard.Width = 100;
            imgCard.VerticalAlignment = VerticalAlignment.Top;
            return imgCard;
        }

        /// <summary>
        /// This returns the card image to display
        /// TODO: This is still a work in progress...
        /// Threaded call to this method results in the same card being 
        /// returned for both user and computer.  Not sure how to resolve.
        /// </summary>
        /// <param name="card"></param>
        /// <returns></returns>
        public String GetCardImage(Card card)
        {
            String defaultCard = @"/Blackjack;component/Resources/QueenOfHearts.jpg";
            String cardImage = @"/Blackjack;component/Resources/Cards/";

            cardImage += card.Suit + "_";

            try
            {
                // get the card image by rank and suit
                if ((int)card.Rank <= 10)
                    cardImage += (int)card.Rank;
                else if ((int)card.Rank == 14)
                    cardImage += "1";
                else if (card.Rank.ToString().Equals("Jack") || card.Rank.ToString().Equals("Queen")
                    || card.Rank.ToString().Equals("King"))
                    cardImage += card.Rank;
                else
                {
                    return defaultCard;
                }

                return cardImage + ".png";
            }
            catch (Exception ex)
            {
                return defaultCard;
            }
        }

        // ---------------------------------------- Helper ----------------------------------------
        // ----------------------------------------------------------------------------------------
        // -------------------------------------- MVC/MVVM ----------------------------------------

        #region INotify
        public event PropertyChangedEventHandler PropertyChanged;

        void OnPropertyChanged([CallerMemberName] String propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }

        #endregion

        #region ICommandEvents
        public ICommand HitMeCommand { get; private set; }

        public ICommand NewGameCommand { get; private set; }

        public ICommand StayCommand { get; private set; }

        public ICommand SaveCommand { get; private set; }

        public ICommand ShowStatisticsCommand { get; private set; }

        #endregion

        // -------------------------------------- MVC/MVVM ----------------------------------------
        // ----------------------------------------------------------------------------------------
    }
}
