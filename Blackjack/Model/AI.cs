﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Blackjack.Model
{
    public class AI
    {
        // --------------------------------------------------------
        // --------------------- Properties -----------------------

        private const double TWENTY_ONE = 21;
        private Player Dealer;
        private Player User;

        // --------------------- Properties -----------------------
        // --------------------------------------------------------
        // --------------------- Functtions -----------------------

        /// <summary>
        /// This is the constructor for the AI
        /// </summary>
        /// <param name="Dealer"></param>
        public AI(Player Dealer, Player User)
        {
            this.Dealer = Dealer;
            this.User = User;
        }

        /// <summary>
        /// This determines if the computer will hit or stay
        /// </summary>
        /// <returns></returns>
        public Boolean HitOrNah()
        {
            if (Dealer.CAN_HIT)
            {
                // if computer has 21, stay
                if (Dealer.SCORE == TWENTY_ONE)
                    return false;

                // if user busted, computer is unable to hit
                if (User.SCORE > TWENTY_ONE)
                    return false;

                // if computer is winning, stay
                if (User.SCORE > Dealer.SCORE)
                    return true;

                // if user has 21
                if (User.SCORE == TWENTY_ONE)
                {
                    if (Dealer.SCORE < TWENTY_ONE)
                        return true;
                    if (Dealer.SCORE == TWENTY_ONE)
                        return false;
                }

                // if computer has lower score than user, determine the odds 
                // of the computer hitting based on how far from 21 it is
                if (User.SCORE < TWENTY_ONE && Dealer.SCORE < TWENTY_ONE)
                {
                    if (Dealer.SCORE < 12)
                        return true;
                    else
                    {
                        double difference = 21 - Dealer.SCORE;
                        double hitOdds = (difference / TWENTY_ONE) * 100;
                        // genereate random number
                        Random rnd = new Random();
                        double randy = rnd.Next(0, 100);

                        if (randy <= hitOdds)
                            return true;
                        else
                            return false;
                    }
                }
            }

            return true;
        }

        // --------------------- Functtions -----------------------
        // --------------------------------------------------------
    }
}
