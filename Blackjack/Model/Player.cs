﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace Blackjack.Model
{
    public class Player : INotifyPropertyChanged
    {
        // --------------------------------------------------------
        // --------------------- Properties -----------------------

        #region properties
        public String NAME { get; set; }
        public List<Card> HAND { get; set; }

        private int _score;
        public int SCORE
        {
            get { return _score; }
            set
            {
                if (value == _score)
                    return;
                _score = value;
                OnPropertyChanged("SCORE");
            }
        }

        private Boolean _bust;
        public Boolean BUST
        {
            get { return _bust; }
            set
            {
                if (value == _bust)
                    return;
                _bust = value;
                OnPropertyChanged("BUST");
            }
        }

        private Visibility _bust_visibility;
        public Visibility BUST_VISIBILITY
        {
            get { return _bust_visibility; }
            set
            {
                if (value == _bust_visibility)
                    return;
                _bust_visibility = value;
                OnPropertyChanged("BUST_VISIBILITY");
            }
        }
        public Boolean CAN_HIT { get; set; }

        public Boolean MY_TURN { get; set; }
        private Visibility _myturn;
        public Visibility MYTURN
        {
            get { return _myturn; }
            set
            {
                if (value == _myturn)
                    return;
                _myturn = value;
                OnPropertyChanged("MYTURN");
            }
        }

        private String _hand_tostring;
        public String HAND_TOSTRING
        {
            get { return _hand_tostring; }
            set
            {
                if (value == _hand_tostring)
                    return;
                _hand_tostring = value;
                OnPropertyChanged("HAND_TOSTRING");
            }
        }

        private int _wins;
        public int WINS
        {
            get { return _wins; }
            set
            {
                if (value == _wins)
                    return;
                _wins = value;
                OnPropertyChanged("WINS");
            }
        }

        private int _loses;
        public int LOSES
        {
            get { return _loses; }
            set
            {
                if (value == _loses)
                    return;
                _loses = value;
                OnPropertyChanged("LOSES");
            }
        }
        #endregion

        // --------------------- Properties -----------------------
        // --------------------------------------------------------
        // ---------------------- Functions -----------------------


        public Player(String name)
        {
            this.NAME = name;
            NewGame();
            WINS = 0;
            LOSES = 0;
        }

        /// <summary>
        /// This resets the players for each new game
        /// </summary>
        public void NewGame()
        {
            HAND = new List<Card>();
            SCORE = 0;
            BUST_VISIBILITY = Visibility.Hidden;
            BUST = false;
            MYTURN = Visibility.Hidden;
            MY_TURN = false;
            CAN_HIT = true;
            HAND_TOSTRING = "";
        }

        /// <summary>
        /// This adds the next card in the deck to the players hand
        /// </summary>
        /// <param name="card"></param>
        public void AddCardToHand(Card card)
        {
            if (!CAN_HIT)
            {
                // show can't hit message
                return;
            }

            if (HAND.Count < 1)
                HAND_TOSTRING += card.Rank;
            else
                HAND_TOSTRING += ", " + card.Rank;

            HAND.Add(card);
            CalculateScore();
        }


        public void ToggleTurn()
        {
            if (MY_TURN)
            {
                MY_TURN = false;
                MYTURN = Visibility.Hidden;
            }
            else
            {
                MY_TURN = true;
                MYTURN = Visibility.Visible;
            }
        }

        /// <summary>
        /// This calculates the total points in a hand of cards
        /// </summary>
        public void CalculateScore()
        {
            int score = 0;
            foreach (Card card in HAND)
                score += card.RankPoints;
             
            SCORE = score;

            CheckForBust();
        }

        /// <summary>
        /// This checks if the users score exceeds 21
        /// </summary>
        public void CheckForBust()
        {
            if (SCORE > 21)
            {
                BUST_VISIBILITY = Visibility.Visible;
                BUST = true;
                CAN_HIT = false;
            }
        }

        // ---------------------- Functions -----------------------
        // --------------------------------------------------------
        // ---------------------- MVC/MVVM ------------------------

        #region INotify
        public event PropertyChangedEventHandler PropertyChanged;

        void OnPropertyChanged([CallerMemberName] String propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }

        #endregion

        // ---------------------- MVC/MVVM ------------------------
        // --------------------------------------------------------
    }
}
