﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;

namespace Blackjack.Model
{
    public class DeckOfCards
    {

        // properties
        public List<Card> Deck;

        public DeckOfCards()
        {
            // initialization
            Deck = new List<Card>();
            List<List<Card>> UnshuffledDeck = new List<List<Card>>();

            List<Card> Hearts = new List<Card>();
            List<Card> Diamonds = new List<Card>();
            List<Card> Spades = new List<Card>();
            List<Card> Clubs = new List<Card>();

            // unnecessary, but whatever
            UnshuffledDeck.Add(Hearts);
            UnshuffledDeck.Add(Diamonds);
            UnshuffledDeck.Add(Spades);
            UnshuffledDeck.Add(Clubs);

            // populate each suite
            for(int suit = 0; suit < 4; suit++)
            {
                for (int rank = 2; rank < 15; rank++) // rank 1 = aceLow, always assume aceHigh initially
                {
                    UnshuffledDeck[suit].Add(new Card(suit, rank));
                }
            }

            // shuffle the deck
            List<int> TakenPositions = new List<int>();
            int deckPosition;
            Random randy = new Random();
            String str = "";
            for (int count = 0; count < 52; count++)
            {
                do
                {
                    deckPosition = randy.Next(0, 52);
                } while (TakenPositions.IndexOf(deckPosition) != -1);
                // set order
                TakenPositions.Add(deckPosition);
                // set shuffled deck order
                Deck.Add(UnshuffledDeck[(int)Math.Floor((Double)(deckPosition / 13))][deckPosition % 13]);
                str += "(" + Deck[count].Rank + " of " + Deck[count].Suit + ")\n ";
            }

            // print
            //MessageBox.Show(str);
        }
    }
}
