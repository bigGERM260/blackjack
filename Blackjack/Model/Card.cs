﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Blackjack.Model
{
    public class Card
    {
        // enumeration
        public enum RankEnum { AceLow = 1, Two = 2, Three = 3, Four = 4,
            Five = 5, Six = 6, Seven = 7, Eight = 8, Nine = 9, Ten = 10,
            Jack = 11, Queen = 12, King = 13,
            AceHigh = 14};

        public enum SuitEnum { Hearts = 0, Diamonds = 1, Spades = 2, Clubs = 3}

        public int RankPoints;

        public Dictionary<RankEnum, int> PointSystem = new Dictionary<RankEnum, int>()
        {
            [RankEnum.AceLow] = 1,
            [RankEnum.Two] = 2,
            [RankEnum.Three] = 3,
            [RankEnum.Four] = 4,
            [RankEnum.Five] = 5,
            [RankEnum.Six] = 6,
            [RankEnum.Seven] = 7,
            [RankEnum.Eight] = 8,
            [RankEnum.Nine] = 9,
            [RankEnum.Ten] = 10,
            [RankEnum.Jack] = 10,
            [RankEnum.Queen] = 10,
            [RankEnum.King] = 10,
            [RankEnum.AceHigh] = 11
        };

        // properties
        public SuitEnum Suit { get; set; }
        public RankEnum Rank { get; set; }

        // constructor
        public Card(int suit, int rank)
        {
            this.Suit = (SuitEnum)suit;

            switch (rank)
            {
                case 14:
                    Rank = (RankEnum)RankEnum.AceHigh;
                    break;
                case int r when (r <= 10 && r > 1):
                    Rank = (RankEnum)r;
                    break;
                case 11:
                    Rank = (RankEnum)RankEnum.Jack;
                    break;
                case 12:
                    Rank = RankEnum.Queen;
                    break;
                case 13:
                    Rank = RankEnum.King;
                    break;
            }

            SetRankPoints();
        }

        public void SetAceHigh()
        {
            if (this.Rank == RankEnum.AceLow)
                this.Rank = RankEnum.AceHigh;

            SetRankPoints();
        }

        public void SetAceLow()
        {
            if (this.Rank == RankEnum.AceHigh)
                this.Rank = RankEnum.AceLow;

            SetRankPoints();
        }


        public void SetRankPoints()
        {
            RankPoints = PointSystem[Rank];
        }

    }
}
