using NUnit.Framework;
using Blackjack.Model;

namespace Tests
{
    public class AITests
    {
        AI ai;

        [SetUp]
        public void Setup()
        {
            // Arrange
            var dealer = new Player("Dealer");
            dealer.NewGame();
            dealer.CAN_HIT = true;
            dealer.SCORE = 21;
            var user = new Player("User");
            user.NewGame();
            user.SCORE = 20;
            ai = new AI(dealer, user);
        }

        [Test]
        public void HitOrNah_DealerScoreEquals21_ReturnFalse()
        {

            // Act
            var result = ai.HitOrNah();

            // Assert
            Assert.IsFalse(result);
        }
    }
}