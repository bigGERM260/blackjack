using Blackjack.Model;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Blackjack.UnitTest
{
    [TestClass]
    public class AITests
    {

        // Naming Convention: MethodName_Scenario_ExpectedBehavior()
        [TestMethod]
        public void HitOrNah_DealerScoreEqual21_ReturnFalse()
        {
            // Triple A testing 

            // Arrange
            var dealer = new Player("Dealer");
            dealer.NewGame();
            dealer.CAN_HIT = true;
            dealer.SCORE = 21;
            var user = new Player("User");
            user.NewGame();
            user.SCORE = 20;
            var ai = new AI(dealer, user);

            // Act
            var result = ai.HitOrNah();

            // Assert
            Assert.IsFalse(result);
        }
    }
}
